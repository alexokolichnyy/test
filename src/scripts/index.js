/** Guida percorso
 * ./ -> stessa cartella del file (in questo caso cartella /src/scripts/)
 * ../ -> cartella padre della cartella corrente (/src/)
 */
import _ from 'lodash';
import '../styles/style.css';
import Bird from '../imgs/bird.jpg';
import 'minigrid';

function component() {
    let element = document.createElement('div');

    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello world!');


    var myIcon = new Image();
    myIcon.src = Bird;
    myIcon.alt = "Bird test";

    element.appendChild(myIcon);

    return element;
}

document.body.appendChild(component());