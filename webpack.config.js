/** Per la generazione dei file output lanciare il comando npx webpack oppure npm NOME SCRIPT
 * se all'interno del file package.json esiste uno script che richiama webpack (es. -> "build" : "webpack" sotto scripts)
 * In questo caso basterà lanciare il comando npm build al posto di npx webpack
 * Questo creera tutti i file di output all'interno della cartella indicata (in questo caso cartella dist)
 */
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    entry: './src/scripts/index.js',
    output: {
        filename: 'js/bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },

};